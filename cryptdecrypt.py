import PySimpleGUI as sg

textColumn = [
    [
        sg.Multiline("TEST. THING.", size=(50,20))
    ],
    [
        sg.Text("TEST", size=(50,20), key="outputtext")
    ],
    [
        sg.Button("Okay"), sg.Button("Quit")
    ],
]

cipherColumn = [
    [sg.Text("A -> "), sg.InputText(size=(2,1), key="A")],
    [sg.Text("B -> "), sg.InputText(size=(2,1), key="B")],
    [sg.Text("C -> "), sg.InputText(size=(2,1), key="C")],
    [sg.Text("D -> "), sg.InputText(size=(2,1), key="D")],
    [sg.Text("E -> "), sg.InputText(size=(2,1), key="E")],
    [sg.Text("F -> "), sg.InputText(size=(2,1), key="F")],
    [sg.Text("G -> "), sg.InputText(size=(2,1), key="G")],
    [sg.Text("H -> "), sg.InputText(size=(2,1), key="H")],
    [sg.Text("I -> "), sg.InputText(size=(2,1), key="I")],
    [sg.Text("J -> "), sg.InputText(size=(2,1), key="J")],
    [sg.Text("K -> "), sg.InputText(size=(2,1), key="K")],
    [sg.Text("L -> "), sg.InputText(size=(2,1), key="L")],
    [sg.Text("M -> "), sg.InputText(size=(2,1), key="M")],
    [sg.Text("N -> "), sg.InputText(size=(2,1), key="N")],
    [sg.Text("O -> "), sg.InputText(size=(2,1), key="O")],
    [sg.Text("P -> "), sg.InputText(size=(2,1), key="P")],
    [sg.Text("Q -> "), sg.InputText(size=(2,1), key="Q")],
    [sg.Text("R -> "), sg.InputText(size=(2,1), key="R")],
    [sg.Text("S -> "), sg.InputText(size=(2,1), key="S")],
    [sg.Text("T -> "), sg.InputText(size=(2,1), key="T")],
    [sg.Text("U -> "), sg.InputText(size=(2,1), key="U")],
    [sg.Text("V -> "), sg.InputText(size=(2,1), key="V")],
    [sg.Text("W -> "), sg.InputText(size=(2,1), key="W")],
    [sg.Text("X -> "), sg.InputText(size=(2,1), key="X")],
    [sg.Text("Y -> "), sg.InputText(size=(2,1), key="Y")],
    [sg.Text("Z -> "), sg.InputText(size=(2,1), key="Z")],
]

layout = [
    [
        sg.Column(textColumn),
        sg.VSeparator(),
        sg.Column(cipherColumn),
    ]
]

window = sg.Window('Decode!', layout)
while True:
    event, values = window.read()
    if event == "Quit":
        break

    encrypted = list(values[0])
    decrypted = list(values[0])
    
    for i in range(0,len(decrypted)):
        if encrypted[i] not in (".", " "):
            print(decrypted[i])
            decrypted[i] = "_"
    
    for key in values.keys():
        if values[key] != "":
            indices = [
                pos for pos, char in enumerate(values[0]) if char == key
            ]
            for ind in indices:
                decrypted[ind] = values[key]

    window["outputtext"].update(value="".join(decrypted))
